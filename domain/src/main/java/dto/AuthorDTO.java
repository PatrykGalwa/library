package dto;

public class AuthorDTO {

    private Long id;

    private String name;

    private String birthPlace;

    public AuthorDTO() {
    }

    public AuthorDTO(Long id, String name, String birthPlace) {
        this.id = id;
        this.name = name;
        this.birthPlace = birthPlace;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setFirstName(String name) {
        this.name = name;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }
}
