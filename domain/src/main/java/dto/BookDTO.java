package dto;

import model.Category;

import java.time.LocalDate;

public class BookDTO {

    private Long id;

    private String title;

    private String isbn;

    private String authorName;

    private Category category;

    private Integer pages;

    private LocalDate releaseDate;

    private String borrower;

    public BookDTO() {
    }

    public BookDTO(Long id, String title, String isbn, String authorName, Category category, Integer pages, LocalDate releaseDate, String borrower) {
        this.id = id;
        this.title = title;
        this.isbn = isbn;
        this.authorName = authorName;
        this.category = category;
        this.pages = pages;
        this.releaseDate = releaseDate;
        this.borrower = borrower;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getBorrower() {
        return borrower;
    }

    public void setBorrower(String borrower) {
        this.borrower = borrower;
    }
}
