package model;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table
public class Borrower implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @NotNull(message = "First name cannot be empty!")
    @Size(min = 2, message = "First name must have minimum 2 digits!")
    @Column(name = "first_name")
    private String firstName;

    @NotNull(message = "Last name cannot be empty!")
    @Size(min = 2, message = "Last name must have minimum 2 digits!")
    @Column(name = "last_name")
    private String lastName;

    @OneToOne
    @JoinColumn(name = "borrower_details_id")
    private BorrowerDetails borrowerDetails;

    @OneToOne(mappedBy = "borrower")
    private User user;

    @OneToMany(mappedBy = "borrower")
    private List<Borrow> borrowList;

    public Borrower(@NotNull(message = "First name cannot be empty!") @Min(value = 2, message = "First name must have minimum 2 digits!") String firstName, @NotNull(message = "Last name cannot be empty!") @Min(value = 2, message = "Last name must have minimum 2 digits!") String lastName, BorrowerDetails borrowerDetails) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.borrowerDetails = borrowerDetails;
    }

    public Borrower() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public BorrowerDetails getBorrowerDetails() {
        return borrowerDetails;
    }

    public void setBorrowerDetails(BorrowerDetails borrowerDetails) {
        this.borrowerDetails = borrowerDetails;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Borrow> getBorrowList() {
        return borrowList;
    }

    public void setBorrowList(List<Borrow> borrowList) {
        this.borrowList = borrowList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Borrower)) return false;
        Borrower borrower = (Borrower) o;
        return getId().equals(borrower.getId()) &&
                getFirstName().equals(borrower.getFirstName()) &&
                getLastName().equals(borrower.getLastName()) &&
                getBorrowerDetails().equals(borrower.getBorrowerDetails());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFirstName(), getLastName(), getBorrowerDetails());
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }
}
