package model;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @NotNull(message = "Email cannot be empty!")
    @Email
    @Column
    private String email;

    @NotNull(message = "Username cannot be empty!")
    @Size(min = 8, max = 20, message = "Username must have between 8 and 20 digits!")
    @Column(name = "user_name")
    private String userName;

    @NotNull(message = "Password cannot be empty!")
    @Size(min = 8, max = 16, message = "Password must have between 8 and 16 digits!")
    @Column(name="password")
    private String password;

    @Column(name="user_role")
    private String role;

    @OneToOne
    @JoinColumn(name = "borrower_id")
    private Borrower borrower;

    public User(@NotNull(message = "Email cannot be empty!") @Email String email, @NotNull(message = "Username cannot be empty!") @Size(min = 8, max = 20, message = "Username must have between 8 and 20 digits!") String userName, @NotNull(message = "Password cannot be empty!") @Size(min = 8, max = 16, message = "Password must have between 8 and 16 digits!") String password, String role, Borrower borrower) {
        this.email = email;
        this.userName = userName;
        this.password = password;
        this.role = role;
        this.borrower = borrower;
    }

    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Borrower getBorrower() {
        return borrower;
    }

    public void setBorrower(Borrower borrower) {
        this.borrower = borrower;
    }
}