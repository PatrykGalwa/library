package model;

public enum Category {
    thriller,
    sensacja,
    kryminal,
    literaturaMlodziezowa,
    dramat,
    komedia,
    tragedia,
    historyczna,
    horror,
    klasyka,
    przygodowa,
    fantastyka,
    fantasy,
    scienceFiction,
    literaturaWspolczesna,
    satyra,
    literaturaObyczajowa,
    romans,
    literaturaPiekna,
    poezja,
    publicystykaLiteracka,
    eseje,
    literaturaPodroznicza,
    literaturaFaktu,
    biografia,
    autobiografia,
    pamietnik,
    literaturaPopularnonaukowa,
    astronomia,
    astrofizyka,
    biznes,
    finanse,
    encyklopedie, slowniki,
    ezoteryka,
    senniki,
    horoskopy,
    filozofia,
    etyka,
    floraIFauna,
    informatyka,
    matematyka,
    historia,
    jezykoznawstwo,
    naukaOLiteraturze,
    naukiPrzyrodnicze,
    naukiSpoleczne,
    popularnonaukowa,
    poradniki,
    poradnikiDlaRodzicow,
    technika,
    literaturaDziecieca,
    interaktywne,
    obrazkowe,
    edukacyjne,
    opowiesciDlaMlodszychDzieci,
    bajki,
    wierszyki,
    piosenki,
    basnie,
    legendy,
    podania,
    historieBiblijne,
    opowiadania,
    powiesci,
    popularnonaukowaDziecieca,
    pozostale,
    inne,
    albumy,
    czasopisma,
    film,
    kino,
    telewizja,
    hobby,
    komiksy,
    kulinaria,
    przepisyKulinarne,
    militaria,
    wojskowosc,
    motoryzacja,
    muzyka,
    religia,
    rekodzielo,
    rozrywka,
    sport,
    sztuka,
    teatr,
    turystyka,
    mapy,
    atlasy,
    zdrowie,
    medycyna,
}
