package model;

import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "borrower_details")
public class BorrowerDetails implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @NotNull(message = "Address cannot be empty!")
    @Size(min = 2, message = "Address must have minimum 2 digits!")
    @Column
    private String address;

    @NotNull(message = "Phone number cannot be empty!")
    @Size(min = 9, message = "Phone number must have minimum 9 digits!")
    @Pattern(regexp="(?<!\\w)(\\(?(\\+|00)?48\\)?)?[ -]?\\d{3}[ -]?\\d{3}[ -]?\\d{3}(?!\\w)", message = "Wrong phone number!")
    @Column
    private String phone;

    @OneToOne(mappedBy = "borrowerDetails")
    private Borrower borrower1;

    public BorrowerDetails(@NotNull(message = "Address cannot be empty!") @Min(value = 2, message = "Address must have minimum 2 digits!") String address, @NotNull(message = "Phone number cannot be empty!") @Min(value = 9, message = "Phone number must have minimum 9 digits!") String phone) {
        this.address = address;
        this.phone = phone;
    }

    public BorrowerDetails() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Borrower getBorrower1() {
        return borrower1;
    }

    public void setBorrower1(Borrower borrower1) {
        this.borrower1 = borrower1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BorrowerDetails)) return false;
        BorrowerDetails that = (BorrowerDetails) o;
        return getId().equals(that.getId()) &&
                getAddress().equals(that.getAddress()) &&
                getPhone().equals(that.getPhone());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getAddress(), getPhone());
    }
}
