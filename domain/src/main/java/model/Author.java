package model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Entity
@Table
public class Author implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @NotNull
    @Size(min = 2, max = 81)
    @Column(name = "first_name")
    private String firstName;

    @NotNull
    @Size(min = 2, max = 81)
    @Column(name = "last_name")
    private String lastName;

    @NotNull
    @Size(min = 2, max = 81)
    @Column(name = "birth_place")
    private String birthPlace;

    @OneToMany(mappedBy = "author")
    private List<Book> book;

    public Author(@NotNull @Size(min = 2, max = 81) String firstName, @NotNull @Size(min = 2, max = 81) String lastName, @NotNull @Size(min = 2, max = 81) String birthPlace) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthPlace = birthPlace;

    }

    public Author() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public List<Book> getBook() {
        return book;
    }

    public void setBook(List<Book> book) {
        this.book = book;
    }
}
