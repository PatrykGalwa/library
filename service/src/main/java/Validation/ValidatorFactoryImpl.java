package Validation;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

public class ValidatorFactoryImpl {
    private static Validator validator;
    private static ValidatorFactoryImpl validatorFactory;

    private ValidatorFactoryImpl() {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    public static ValidatorFactoryImpl getInstance() {
        if (validatorFactory == null) {
            validatorFactory = new ValidatorFactoryImpl();
        }
        return validatorFactory;
    }

    public Validator getValidator() {
        return validator;
    }
}
