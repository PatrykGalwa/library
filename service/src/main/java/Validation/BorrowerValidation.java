package Validation;

import model.Borrower;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class BorrowerValidation {
    private static Validator validator;
    private final List<String> errors;

    public BorrowerValidation() {
        validator = ValidatorFactoryImpl.getInstance().getValidator();
        this.errors = new ArrayList<>();
    }

    public List<String> checkBorrowerValidation(Borrower borrower) {
        Set<ConstraintViolation<Borrower>> constraintViolations = validator.validate(borrower);
        for(ConstraintViolation<Borrower> constraintViolation : constraintViolations) {
            errors.add(constraintViolation.getMessage());
        }
        return errors;
    }
}
