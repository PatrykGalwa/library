package Validation;

import model.Author;
import model.User;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class AuthorValidation {
    private static Validator validator;
    private final List<String> errors;

    public AuthorValidation() {
        validator = ValidatorFactoryImpl.getInstance().getValidator();
        this.errors = new ArrayList<>();
    }

    public List<String> checkAuthorValidation(Author author) {
        Set<ConstraintViolation<Author>> constraintViolations = validator.validate(author);
        for(ConstraintViolation<Author> constraintViolation : constraintViolations) {
            errors.add(constraintViolation.getMessage());
        }
        return errors;
    }
}
