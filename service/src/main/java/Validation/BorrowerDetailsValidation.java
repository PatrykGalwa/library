package Validation;

import model.BorrowerDetails;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class BorrowerDetailsValidation {
    private static Validator validator;
    private final List<String> errors;

    public BorrowerDetailsValidation() {
        validator = ValidatorFactoryImpl.getInstance().getValidator();
        this.errors = new ArrayList<>();
    }

    public List<String> checkBorrowerDetailsValidation(BorrowerDetails borrowerDetails) {
        Set<ConstraintViolation<BorrowerDetails>> constraintViolations = validator.validate(borrowerDetails);
        for(ConstraintViolation<BorrowerDetails> constraintViolation : constraintViolations) {
            errors.add(constraintViolation.getMessage());
        }
        return errors;
    }
}
