package Validation;

import EntityService.UserService;
import model.User;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class UserValidation {
    private static Validator validator;
    private final List<String> errors;
    private final StringBuilder builder;
    private final UserService userService;

    public UserValidation() {
        validator = ValidatorFactoryImpl.getInstance().getValidator();
        this.errors = new ArrayList<>();
        this.builder = new StringBuilder();
        this.userService = UserService.getInstance();
    }

    public List<String> checkUserValidation(User user) {
        Set<ConstraintViolation<User>> constraintViolations = validator.validate(user);
        for(ConstraintViolation<User> constraintViolation : constraintViolations) {
            errors.add(constraintViolation.getMessage());
        }
        String email = user.getEmail();
        String userName = user.getUserName();

        User emailCheck = userService.getUserByEmail(email);
        User userNameCheck = userService.getUserByUserName(userName);
        if(emailCheck != null || userNameCheck != null) {
            errors.add("Typed email or username exists!");
        }
        return errors;
    }
}
