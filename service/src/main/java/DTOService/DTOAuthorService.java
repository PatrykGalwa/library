package DTOService;

import EntityService.AuthorService;
import dto.AuthorDTO;

import java.util.List;
import java.util.stream.Collectors;

public class DTOAuthorService {
    private static DTOAuthorService dtoAuthorService;
    private final AuthorService authorService;

    private DTOAuthorService() {
        this.authorService = AuthorService.getInstance();
    }

    public static DTOAuthorService getInstance() {
        if (dtoAuthorService == null) {
            dtoAuthorService = new DTOAuthorService();
        }
        return dtoAuthorService;
    }

    public List<AuthorDTO> getAuthorDTOList() {
        return authorService.getList().stream()
                .map(x -> new AuthorDTO(x.getId(),
                        x.getFirstName() + " " + x.getLastName(),
                        x.getBirthPlace()))
                .collect(Collectors.toList());
    }
}
