package DTOService;

import EntityService.BookService;
import dto.BookDTO;
import model.Book;

import java.util.List;
import java.util.stream.Collectors;

public class DTOBookService {
    private static DTOBookService dtoBookService;
    private final BookService bookService;

    private DTOBookService() {
        this.bookService = BookService.getInstance();
    }

    public static DTOBookService getInstance() {
        if (dtoBookService == null) {
            dtoBookService = new DTOBookService();
        }
        return dtoBookService;
    }

    public List<BookDTO> getBookDTOList() {
        return bookService.getBooksList().stream()
                .map(x -> new BookDTO(x.getId(),
                        x.getTitle(),
                        x.getIsbn(),
                        x.getAuthor().getFirstName() + " " + x.getAuthor().getLastName(),
                        x.getCategory(),
                        x.getPages(),
                        x.getReleaseDate(),
                        x.getBorrowList().get(0).getBorrower() == null ? "-" : x.getBorrowList().get(0).getBorrower().getFirstName() + " " + x.getBorrowList().get(0).getBorrower().getFirstName()))
                .collect(Collectors.toList());
    }
}
