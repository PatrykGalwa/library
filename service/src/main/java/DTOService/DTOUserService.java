package DTOService;

import EntityService.BorrowerService;
import EntityService.UserService;
import dto.UserDTO;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import static Repository.GenericRepository.em;

public class DTOUserService {
    private static DTOUserService dtoUserService;
    private BorrowerService borrowerService;
    private UserService userService;

    private DTOUserService() {
        this.userService = UserService.getInstance();
    }

    public static DTOUserService getInstance() {
        if (dtoUserService == null) {
            dtoUserService = new DTOUserService();
        }
        return dtoUserService;
    }

//    public UserDTO mapOnUserDTO(User user) {
//        Borrower borrower = borrowerService.getBorrower(user.getBorrower().getId());
//        return new UserDTO(user.getId(), borrower.getFirstName() + " " + borrower.getLastName());
//    }
}
