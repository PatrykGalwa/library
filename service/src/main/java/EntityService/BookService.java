package EntityService;

import Repository.BookRepository;
import model.Book;

import java.util.Collections;
import java.util.List;

public class BookService {
    private static BookService bookService;
    private final BookRepository bookRepository;

    private BookService() {
        this.bookRepository = BookRepository.getInstance();
    }

    public static BookService getInstance() {
        if (bookService == null) {
            bookService = new BookService();
        }
        return bookService;
    }

    public void saveEntity(Book book) {
        try {
            bookRepository.create(book);
        } catch (Exception e) {
            System.out.println("błąd book");
        }
    }

    public List<Book> getBooksList() {
        if (bookRepository.getEntityList() == null) {
            return Collections.emptyList();
        }
        return bookRepository.getEntityList();
    }

    public Book getBook(Long id) {
        return bookRepository.read(id);
    }

    public void updateBook(Book book) {
        bookRepository.update(book);
    }

    public void deleteBook(Book book) {
        bookRepository.delete(book);
    }
}
