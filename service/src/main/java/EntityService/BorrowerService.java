package EntityService;

import Repository.BorrowerRepository;
import model.Borrower;

import java.util.List;

public class BorrowerService {
    private static BorrowerService borrowerService;
    private BorrowerRepository borrowerRepository;

    private BorrowerService() {
    }

    public static BorrowerService getInstance() {
        if (borrowerService == null) {
            borrowerService = new BorrowerService();
        }
        return borrowerService;
    }

    public void saveEntity(Borrower borrower) {
        borrowerRepository = BorrowerRepository.getInstance();
        try {
            borrowerRepository.create(borrower);
        } catch (Exception e) {
            System.out.println("Błąd Borrower");
        }
    }

    public List<Borrower> getBorrowerList() {
        return borrowerRepository.getEntityList();
    }

    public Borrower getBorrower(Long id) {
        return borrowerRepository.read(id);
    }
}
