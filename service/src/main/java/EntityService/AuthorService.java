package EntityService;

import Repository.AuthorRepository;
import model.Author;

import java.util.List;

public class AuthorService {
    private static AuthorService authorService;
    private final AuthorRepository authorRepository;

    private AuthorService(){
        this.authorRepository = AuthorRepository.getInstance();
    }

    public static AuthorService getInstance() {
        if (authorService == null) {
            authorService = new AuthorService();
        }
        return authorService;
    }

    public void saveEntity(Author author) {
        try {
            authorRepository.create(author);
        } catch (Exception e) {
            System.out.println("błąd author");
        }
    }

    public List<Author> getList() {
        return authorRepository.getEntityList();
    }

    public Author getAuthor(Long id) {
        return authorRepository.read(id);
    }
}
