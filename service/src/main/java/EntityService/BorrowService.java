package EntityService;

import Repository.BorrowRepository;
import model.Borrow;

import java.util.List;

public class BorrowService {
    private static BorrowService borrowService;
    private BorrowRepository borrowRepository;

    private BorrowService() {
    }

    public static BorrowService getInstance() {
        if (borrowService == null) {
            borrowService = new BorrowService();
        }
        return borrowService;
    }

    public void saveEntity(Borrow borrow) {
        borrowRepository = BorrowRepository.getInstance();
        try {
            borrowRepository.create(borrow);
        } catch (Exception e) {
            System.out.println("bład Borrow");
        }
    }

    public List<Borrow> getBorrowList() {
        return borrowRepository.getEntityList();
    }
}
