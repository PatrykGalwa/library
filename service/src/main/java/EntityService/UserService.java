package EntityService;

import Repository.UserRepository;
import dto.UserDTO;
import model.User;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;

import static Repository.GenericRepository.em;

public class UserService {
    private static UserService userService;
    private UserRepository userRepository;

    private UserService() {
        this.userRepository = UserRepository.getInstance();
    }

    public static UserService getInstance() {
        if (userService == null) {
            userService = new UserService();
        }
        return userService;
    }

    public void saveEntity(User user) {
        try {
            userRepository.create(user);
        } catch (Exception e) {
            System.out.println("błąd user");
        }
    }

    public List<User> getUserList() {
        return userRepository.getEntityList();
    }

    public User getUserByUserNameOrEmail(String loginOrEmail) {
        try {
            TypedQuery<User> query = em.createQuery("SELECT u from User u where u.userName = :fieldVal or u.email = :fieldVal", User.class);
            query.setParameter("fieldVal", loginOrEmail);
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public User getUserByEmail(String email) {
        try {
            TypedQuery<User> query = em.createQuery("SELECT u FROM User u where u.email = :fieldVal", User.class);
            query.setParameter("fieldVal", email);
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public User getUserByUserName(String userName) {
        try {
            TypedQuery<User> query = em.createQuery("SELECT u from User u where u.userName = :fieldVal", User.class);
            query.setParameter("fieldVal", userName);
            return query.getSingleResult();
        }catch (NoResultException e) {
            return null;
        }
    }

    public boolean checkLoginMatch(String login, String password) {
        User databaseUser = getUserByUserNameOrEmail(login);
        return databaseUser != null && databaseUser.getPassword().equals(password);
    }
}