package EntityService;

import Repository.BorrowerDetailsRepository;
import model.BorrowerDetails;

import java.util.List;

public class BorrowerDetailsService {
    private static BorrowerDetailsService borrowerDetailsService;
    private BorrowerDetailsRepository borrowerDetailsRepository;

    private BorrowerDetailsService() {
    }

    public static BorrowerDetailsService getInstance() {
        if (borrowerDetailsService == null) {
            borrowerDetailsService = new BorrowerDetailsService();
        }
        return borrowerDetailsService;
    }

    public void saveEntity(BorrowerDetails borrowerDetails) {
        borrowerDetailsRepository = BorrowerDetailsRepository.getInstance();
        try {
            borrowerDetailsRepository.create(borrowerDetails);
        } catch (Exception e) {
            System.out.println("Błąd Borrower Details");
        }
    }

    public List<BorrowerDetails> getBorrowerDetailsList() {
        return borrowerDetailsRepository.getEntityList();
    }
}
