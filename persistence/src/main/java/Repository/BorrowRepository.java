package Repository;

import model.Borrow;

public class BorrowRepository extends GenericRepository<Borrow, Long> {
    private static BorrowRepository borrowRepository;

    private BorrowRepository() {
    }

    public static BorrowRepository getInstance() {
        if (borrowRepository == null) {
            borrowRepository = new BorrowRepository();
        }
        return borrowRepository;
    }
}
