package Repository;

import dto.UserDTO;
import model.User;
import org.hibernate.annotations.NamedNativeQueries;
import org.hibernate.annotations.NamedNativeQuery;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public class UserRepository extends GenericRepository<User, Long>{
    private static UserRepository userRepository;

    private UserRepository(){
    }

    public static UserRepository getInstance() {
        if (userRepository == null) {
            userRepository = new UserRepository();
        }
        return userRepository;
    }
}
