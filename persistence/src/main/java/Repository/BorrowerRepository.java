package Repository;

import model.Borrower;
import model.BorrowerDetails;

import javax.persistence.EntityManager;

public class BorrowerRepository extends GenericRepository<Borrower, Long> {
    private static BorrowerRepository borrowerDetails;

    private BorrowerRepository() {
    }

    public static BorrowerRepository getInstance() {
        if (borrowerDetails == null) {
            borrowerDetails = new BorrowerRepository();
        }
        return borrowerDetails;
    }
}
