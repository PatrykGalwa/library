package Repository;

import model.BorrowerDetails;

import javax.persistence.EntityManager;

public class BorrowerDetailsRepository extends GenericRepository<BorrowerDetails, Long> {
    private static BorrowerDetailsRepository borrowerDetailsRepository;

    private BorrowerDetailsRepository() {
    }

    public static BorrowerDetailsRepository getInstance() {
        if (borrowerDetailsRepository == null) {
            borrowerDetailsRepository =  new BorrowerDetailsRepository();
        }
        return borrowerDetailsRepository;
    }
}
