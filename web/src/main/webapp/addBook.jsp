<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <title>Add book form</title>
</head>
<body>
<jsp:include page="/WEB-INF/fragments/header.jspf"/>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-8 mx-auto">
            <div class="card card-signin my-5">
                <div class="card-body">
                    <h5 class="card-title text-center">Add book</h5>
                    <form method="post" action="AddBookServlet">
                        <div class="row">
                            <div class="col">
                                <input type="hidden" class="form-control" placeholder="Borrow" name="action" value="false">
                                <label for="author">Author</label>
                                <select id="author" name="action">
                                    <c:forEach var="author" items="${requestScope.author}">
                                        <option value="${author.getId()}">${author.getName()}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="col">
                                <label for="category">Category</label>
                                <select id="category" name="action">
                                    <c:forEach var="category" items="${requestScope.category}">
                                        <option>${category}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input id="title" type="text" class="form-control" placeholder="Title" name="action">
                                <label for="title">Title</label>
                            </div>
                            <div class="col">
                                <input id="releaseDate" type="date" class="form-control" placeholder="Release Date" name="action">
                                <label for="releaseDate">Release Date</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input id="isbn" type="text" class="form-control" placeholder="ISBN" name="action">
                                <label for="isbn">ISBN</label>
                            </div>
                            <div class="col">
                                <input id="pages" type="text" class="form-control" placeholder="Pages" name="action">
                                <label for="pages">Pages</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <textarea id="summary" type="text" class="form-control" placeholder="Summary" name="action"></textarea>
                                <label for="summary">Summary</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button type="submit" class="btn btn-primary">Add</button>
                                <a href="home.jsp"><button type="button" class="btn btn-primary">Cancel</button></a>
                            </div>
                            <div class="col"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/fragments/footer.jspf" %>
</body>
</html>