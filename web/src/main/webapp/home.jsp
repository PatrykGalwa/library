<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <title>Home page</title>
</head>
<body>
<jsp:include page="/WEB-INF/fragments/header.jspf"/>
<form action="HomeServlet" method="post" class="form-group">
    <table class="table table-dark table-striped table-sm">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">ISBN</th>
            <th scope="col">Author</th>
            <th scope="col">Category</th>
            <th scope="col">Release</th>
            <th scope="col">Pages</th>
            <th scope="col">Borrower</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
            <c:forEach var="book" items="${requestScope.books}">
                <tr>
                    <td>${book.getId()}</td>
                    <td>${book.getTitle()}</td>
                    <td>${book.getIsbn()}</td>
                    <td>${book.getAuthor().getFirstName()} ${book.getAuthor().getLastName()}</td>
                    <td>${book.getCategory()}</td>
                    <td>${book.getReleaseDate()}</td>
                    <td>${book.getPages()}</td>
                    <c:choose>
                        <c:when test="${!book.getBorrow()}"> <td>-</td></c:when>
                        <c:otherwise>
                            <td>${book.getBorrowList().get(0).getBorrower().getFirstName()} ${book.getBorrowList().get(0).getBorrower().getLastName()}</td>
                        </c:otherwise>
                    </c:choose>
                    <td><input type="radio" name="commit" value="${book.getId()}"></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <div class="form-group">
        <c:if test="${sessionScope.user.getRole().equals(\"admin\")}">
        <button type="submit" name="action" value="add">Add book</button>
        <button type="submit" name="action" value="delete">Delete book</button>
        <button type="submit" name="action" value="edit">Edit book</button>
        </c:if>
        <button type="submit" name="action" value="show">Show book</button>
    </div>
</form>
<%@ include file="/WEB-INF/fragments/footer.jspf" %>
</body>
</html>