<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <title>Show book</title>
</head>
<body>
<jsp:include page="/WEB-INF/fragments/header.jspf"/>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-8 mx-auto">
            <div class="card card-signin my-5">
                <div class="card-body">
                    <h5 class="card-title text-center">${requestScope.book.getTitle()}</h5>
                    <div class="row">
                        <div class="col">
                            <label for="name">Author</label>
                            <input id="name" type="text" value="${requestScope.book.getAuthor().getFirstName()} ${requestScope.book.getAuthor().getLastName()}" disabled>
                            <label for="date">Release date</label>
                            <input id="date" type="text" value="${requestScope.book.getReleaseDate()}" disabled>
                            <label for="category">Category</label>
                            <input id="category" type="text" value="${requestScope.book.getCategory()}" disabled>
                            <label for="isbn">ISBN</label>
                            <input id="isbn" type="text" value="${requestScope.book.getIsbn()}" disabled>
                            <label for="pages">Pages</label>
                            <input id="pages" type="text" value="${requestScope.book.getPages()}" disabled>
                        </div>
                        <div class="col">
                            <div class="row">
                                <label for="summary">Summary</label>
                                <div id="summary"><br>${requestScope.book.getSummary()}</div>
                            </div>
                            <br>
                            <div class="row">
                                <form action="ShowBookServlet" method="get">
                                    <label for="borrower">Borrower</label>
                                    <c:choose>
                                        <c:when test="${!requestScope.book.getBorrow()}">
                                            <select id="borrower" >
                                                <option>${sessionScope.user.getBorrower().getFirstName()} ${sessionScope.user.getBorrower().getLastName()}</option>
                                            </select>
                                            <button type="submit" class="btn btn-primary" name="action" value="borrow">Borrow</button>
                                            <input type="hidden" name="book" value="${requestScope.book.getId()}" >
                                        </c:when>
                                        <c:otherwise>
                                            <input id="borrower" value="${requestScope.book.getBorrowList().get(requestScope.book.getBorrowList().size() - 1).getBorrower().getFirstName()} ${requestScope.book.getBorrowList().get(requestScope.book.getBorrowList().size() - 1).getBorrower().getLastName()}" disabled>
                                            <c:if test="${sessionScope.user.getBorrower().equals(requestScope.book.getBorrowList().get(requestScope.book.getBorrowList().size() - 1)
                                            .getBorrower())}">
                                                <br>
                                                <button type="submit" class="btn btn-primary" name="action" value="leave">Leave</button>
                                                <input type="hidden" name="book" value="${requestScope.book.getId()}" >
                                            </c:if>
                                        </c:otherwise>
                                    </c:choose>
                                </form>
                            </div>
                        </div>
                        <div class="col"><img src=images/book.jpg alt="Book" width="200" height="150"></div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <br>
                            <a href="HomeServlet"><button type="button" class="btn btn-lg btn-primary btn-block text-uppercase">Back to home page</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/fragments/footer.jspf" %>
</body>
</html>