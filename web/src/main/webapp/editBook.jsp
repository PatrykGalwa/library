<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <title>Edit book form</title>
</head>
<body>
<jsp:include page="/WEB-INF/fragments/header.jspf"/>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-8 mx-auto">
            <div class="card card-signin my-5">
                <div class="card-body">
                    <h5 class="card-title text-center">Edit book</h5>
                    <form method="post" action="EditBookServlet">
                        <div class="row">
                            <div class="col">
                                <label for="author">Author</label>
                                <select id="author" name="authorId">
                                    <c:forEach var="author" items="${requestScope.author}">
                                        <option value="${author.getId()}">${author.getName()}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="col">
                                <label for="category">Category</label>
                                <select id="category" name="categories">
                                    <c:forEach var="category" items="${requestScope.category}">
                                        <option value="${category}">${category}</option>
                                    </c:forEach>
                                </select>
                                <input type="hidden" name="booksId" value="${requestScope.book.getId()}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label for="title">Title</label>
                                <input id="title" type="text" class="form-control" placeholder="Title" name="title" value="${requestScope.book.getTitle()}">
                            </div>
                            <div class="col">
                                <label for="releaseDate">Release Date</label>
                                <input id="releaseDate" type="date" class="form-control" placeholder="Release Date" name="date" value="${requestScope.book.getReleaseDate()}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label for="isbn">ISBN</label>
                                <input id="isbn" type="text" class="form-control" placeholder="ISBN" name="isbn" value="${requestScope.book.getIsbn()}">
                            </div>
                            <div class="col">
                                <label for="pages">Pages</label>
                                <input id="pages" type="text" class="form-control" placeholder="Pages" name="pages" value="${requestScope.book.getPages()}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label for="summary">Summary</label>
                                <input id="summary" type="text" class="form-control" placeholder="Summary" name="summary" value="${requestScope.book.getSummary()}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="HomeServlet"><button type="button" class="btn btn-primary">Cancel</button></a>
                            </div>
                            <div class="col"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/fragments/footer.jspf" %>
</body>
</html>