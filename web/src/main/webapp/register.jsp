<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <title>Registration form</title>
</head>
<body>
<jsp:include page="/WEB-INF/fragments/header.jspf"/>
<div class="container">
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin my-5">
                <div class="card-body">
                    <h5 class="card-title text-center">Register</h5>
                    <form class="form-signin" method="post" action="RegisterServlet">
                        <c:forEach var="err" items="${requestScope.BD}">
                            <div class="font-weight-bold text-danger">${err}</div>
                        </c:forEach>
                        <c:forEach var="err1" items="${requestScope.B}">
                            <div class="font-weight-bold text-danger">${err1}</div>
                        </c:forEach>
                        <c:forEach var="err2" items="${requestScope.U}">
                            <div class="font-weight-bold text-danger">${err2}</div>
                        </c:forEach>
                        <br>
                        <div class="form-label-group">
                            <input id="email" type="email" class="form-control" placeholder="Email" name="action" value="${requestScope.user[0]}">
                            <label for="email">Email address</label>
                        </div>
                        <div class="form-label-group">
                            <input id="login" type="text" class="form-control" placeholder="Login" name="action" value="${requestScope.user[1]}">
                            <label for="login">Login</label>
                        </div>
                        <div class="form-label-group">
                            <input id="password" type="password" class="form-control" placeholder="Password" name="action" value="${requestScope.user[2]}">
                            <label for="password">Password</label>
                            <input type="hidden" name="action" value="user">
                        </div>
                        <div class="form-label-group">
                            <input id="firstName" type="text" class="form-control" placeholder="First name" name="action" value="${requestScope.user[4]}">
                            <label for="firstName">First name</label>
                        </div>
                        <div class="form-label-group">
                            <input id="lastName" type="text" class="form-control" placeholder="Last name" name="action" value="${requestScope.user[5]}">
                            <label for="lastName">Last name</label>
                        </div>
                        <div class="form-label-group">
                            <input id="address" type="text" class="form-control" placeholder="Address" name="action" value="${requestScope.user[6]}">
                            <label for="address">Address</label>
                        </div>
                        <div class="form-label-group">
                            <input id="phone" type="text" class="form-control" placeholder="Phone number" name="action" value="${requestScope.user[7]}">
                            <label for="phone">Phone number</label>
                        </div>
                        <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Register</button>
                        <br>
                        <a href="index.jsp"><button class="btn btn-lg btn-primary btn-block text-uppercase" type="button">Back to login page</button></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/fragments/footer.jspf" %>
</body>
</html>