<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <title>Delete book form</title>
</head>
<body>
<jsp:include page="/WEB-INF/fragments/header.jspf"/>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-8 mx-auto">
            <div class="card card-signin my-5">
                <div class="card-body">
                    <h5 class="card-title text-center">Delete book</h5>
                    <form action="DeleteBookServlet" method="post">
                    <div class="row">
                        <div class="col">
                            <label for="name">Author</label>
                            <input id="name" type="text" value="${requestScope.book1.getAuthor().getFirstName()} ${requestScope.book1.getAuthor().getLastName()}" disabled>
                            <label for="date">Release date</label>
                            <input id="date" type="text" value="${requestScope.book1.getReleaseDate()}" disabled>
                            <label for="category">Category</label>
                            <input id="category" type="text" value="${requestScope.book1.getCategory()}" disabled>
                            <label for="isbn">ISBN</label>
                            <input id="isbn" type="text" value="${requestScope.book1.getIsbn()}" disabled>
                            <label for="pages">Pages</label>
                            <input id="pages" type="text" value="${requestScope.book1.getPages()}" disabled>
                        </div>
                        <div class="col">
                            <div class="row">
                                <label for="summary">Summary</label>
                                <div id="summary"><br>${requestScope.book1.getSummary()} </div>
                            </div>
                            <br>
                            <c:if test="${requestScope.book1.getBorrow()}">
                            <div class="row">
                                <label for="borrower">Borrower</label>
                                <input id="borrower" type="text" value="${requestScope.book1.getBorrowList().get(requestScope.book1.getBorrowList().size() - 1).getBorrower().getFirstName()} ${requestScope.book1.getBorrowList().get(requestScope.book1.getBorrowList().size() - 1).getBorrower().getLastName()}" disabled>
                            </div>
                            </c:if>
                        </div>
                            <div class="col"><img src=images/book.jpg alt="Book" width="200" height="150"></div>
                        </div>
                        <c:if test="${!requestScope.book1.getBorrow()}">
                        <button type="submit" name="dead" value="${requestScope.book1.getId()}" class="btn-primary">Delete</button>
                        </c:if>
                        <a href="HomeServlet"><button type="button" class="btn-primary">Cancel</button></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/fragments/footer.jspf" %>
</body>
</html>