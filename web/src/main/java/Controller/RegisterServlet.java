package Controller;

import EntityService.BorrowerDetailsService;
import EntityService.BorrowerService;
import EntityService.UserService;
import Validation.BorrowerDetailsValidation;
import Validation.BorrowerValidation;
import Validation.UserValidation;
import model.Borrower;
import model.BorrowerDetails;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {

    private final BorrowerDetailsService borrowerDetailsService;
    private final BorrowerService borrowerService;
    private final UserService userService;

    public RegisterServlet() {
        this.borrowerDetailsService = BorrowerDetailsService.getInstance();
        this.borrowerService = BorrowerService.getInstance();
        this.userService = UserService.getInstance();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("user", null);
        req.setAttribute("BD", null);
        req.setAttribute("B", null);
        req.setAttribute("U", null);

        String[] values = req.getParameterValues("action");

        BorrowerDetails borrowerDetails = new BorrowerDetails(values[6], values[7]);
        Borrower borrower = new Borrower(values[4], values[5], borrowerDetails);
        User user = new User(values[0], values[1], values[2], values[3], borrower);

        List<String> errBD = new BorrowerDetailsValidation().checkBorrowerDetailsValidation(borrowerDetails);
        List<String> errB = new BorrowerValidation().checkBorrowerValidation(borrower);
        List<String> errU = new UserValidation().checkUserValidation(user);

        if (errBD.isEmpty() && errB.isEmpty() && errU.isEmpty()) {
            borrowerDetailsService.saveEntity(borrowerDetails);
            borrowerService.saveEntity(borrower);
            userService.saveEntity(user);
            req.setAttribute("done", "Register done!");
            req.getRequestDispatcher("index.jsp").forward(req, resp);
        } else {

            req.setAttribute("user", values);
            req.setAttribute("BD", errBD);
            req.setAttribute("B", errB);
            req.setAttribute("U", errU);
            values = null;
            errB = null;
            errBD = null;
            errU = null;
            req.getRequestDispatcher("register.jsp").forward(req, resp);
        }
    }
}
