package Controller;

import DTOService.DTOBookService;
import DTOService.DTOUserService;
import EntityService.BookService;
import EntityService.UserService;
import Repository.BookRepository;
import dto.UserDTO;
import model.Book;
import model.User;

import javax.jws.soap.SOAPBinding;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
    private final UserService userService;
    private final DTOUserService dtoUserService;
    private final BookService bookService;

    public LoginServlet() {
        this.userService = UserService.getInstance();
        this.dtoUserService = DTOUserService.getInstance();
        this.bookService = BookService.getInstance();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (userService.checkLoginMatch(req.getParameter("login"), req.getParameter("password"))) {
            User user = userService.getUserByUserNameOrEmail(req.getParameter("login"));
            HttpSession session = req.getSession();
            session.setAttribute("user", user);
            resp.sendRedirect("HomeServlet");
        } else {
            req.setAttribute("error", "Wrong Login/Email or password");
            req.getRequestDispatcher("index.jsp").forward(req, resp);
        }
    }
}
