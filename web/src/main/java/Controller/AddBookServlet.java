package Controller;

import DTOService.DTOAuthorService;
import EntityService.AuthorService;
import EntityService.BookService;
import dto.AuthorDTO;
import model.Author;
import model.Book;
import model.Category;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@WebServlet("/AddBookServlet")
public class AddBookServlet extends HttpServlet {

    private final AuthorService authorService;
    private final DTOAuthorService dtoAuthorService;
    private final BookService bookService;

    public AddBookServlet() {
        this.authorService = AuthorService.getInstance();
        this.dtoAuthorService = DTOAuthorService.getInstance();
        this.bookService = BookService.getInstance();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] book = request.getParameterValues("action");
        Author author = authorService.getAuthor(Long.parseLong(book[1]));
        Book writableBook = new Book(Boolean.getBoolean(book[0]),
                Category.valueOf(book[2]),
                book[5],
                Integer.parseInt(book[6]),
                LocalDate.parse(book[4]),
                book[7],
                book[3],
                author);

        bookService.saveEntity(writableBook);
        response.sendRedirect("HomeServlet");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<AuthorDTO> authors = dtoAuthorService.getAuthorDTOList();
        request.setAttribute("author", authors);
        Category[] category = Category.values();
        request.setAttribute("category", category);
        request.getRequestDispatcher("addBook.jsp").forward(request, response);
    }
}
