package Controller;

import EntityService.AuthorService;
import EntityService.BookService;
import model.Author;
import model.Book;
import model.Category;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Enumeration;

@WebServlet("/EditBookServlet")
public class EditBookServlet extends HttpServlet {
    private final BookService bookService;
    private final AuthorService authorService;

    public EditBookServlet() {
        this.bookService = BookService.getInstance();
        this.authorService = AuthorService.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Book previousBook = bookService.getBook(Long.parseLong(req.getParameter("booksId")));
        Author author = authorService.getAuthor(Long.parseLong(req.getParameter("authorId")));

        previousBook.setCategory(Category.valueOf(req.getParameter("categories")));
        previousBook.setIsbn(req.getParameter("isbn"));
        previousBook.setPages(Integer.parseInt(req.getParameter("pages")));
        previousBook.setReleaseDate(LocalDate.parse(req.getParameter("date")));
        previousBook.setSummary(req.getParameter("summary"));
        previousBook.setTitle(req.getParameter("title"));
        previousBook.setAuthor(author);
        bookService.updateBook(previousBook);
        resp.sendRedirect("HomeServlet");
    }
}
