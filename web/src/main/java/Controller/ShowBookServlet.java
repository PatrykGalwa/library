package Controller;

import EntityService.BookService;
import EntityService.BorrowService;
import model.Book;
import model.Borrow;
import model.Borrower;
import model.User;

import javax.jws.soap.SOAPBinding;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

@WebServlet("/ShowBookServlet")
public class ShowBookServlet extends HttpServlet {
    private final BookService bookService;
    private final BorrowService borrowService;


    public ShowBookServlet() {
        this.bookService = BookService.getInstance();
        this.borrowService = BorrowService.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String action = req.getParameter("action");
        Long bookId = Long.parseLong(req.getParameter("book"));
        Book book = bookService.getBook(bookId);

        switch (action) {
            case "borrow":
                User user = (User) req.getSession(false).getAttribute("user");
                book.setBorrow(true);
                bookService.updateBook(book);
                borrowService.saveEntity(new Borrow(book, user.getBorrower(), LocalDate.now()));
                req.getRequestDispatcher("HomeServlet").forward(req, resp);
                return;
            case "leave":
                book.setBorrow(false);
                bookService.updateBook(book);
                req.getRequestDispatcher("HomeServlet").forward(req, resp);
                return;
            default:
                req.getRequestDispatcher("HomeServlet").forward(req, resp);
                return;
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String bookId = req.getParameter("commit");
        Book book = bookService.getBook(Long.parseLong(bookId));
        req.setAttribute("book", book);
        req.getRequestDispatcher("showBook.jsp").forward(req, resp);
    }
}
