package Controller;

import EntityService.BookService;
import model.Book;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

@WebServlet("/DeleteBookServlet")
public class DeleteBookServlet extends HttpServlet {
    private final BookService bookService;

    public DeleteBookServlet() {
        this.bookService = BookService.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.parseLong(req.getParameter("dead"));
        Book deletedBook = bookService.getBook(id);
        bookService.deleteBook(deletedBook);
        resp.sendRedirect("HomeServlet");
    }
}
