package Controller;

import DTOService.DTOAuthorService;
import DTOService.DTOBookService;
import EntityService.BookService;
import Repository.AuthorRepository;
import com.sun.deploy.net.HttpRequest;
import dto.AuthorDTO;
import dto.BookDTO;
import model.Author;
import model.Book;
import model.Category;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/HomeServlet")
public class HomeServlet extends HttpServlet {

    private final BookService bookService;
    private final DTOAuthorService dtoAuthorService;

    public HomeServlet() {
        this.bookService = BookService.getInstance();
        this.dtoAuthorService = DTOAuthorService.getInstance();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] actions = req.getParameterValues("action");
        for (String a : actions) {
            switch (a) {
                case "add" :
                    resp.sendRedirect("AddBookServlet");
                    break;
                case "edit" :
                    if(req.getParameter("commit") == null) {
                        resp.sendRedirect("HomeServlet");
                    }
                    List<AuthorDTO> authorList = dtoAuthorService.getAuthorDTOList();
                    req.setAttribute("author", authorList);
                    Category[] categories = Category.values();
                    req.setAttribute("category", categories);
                    Long bookId = Long.parseLong(req.getParameter("commit"));
                    Book book = bookService.getBook(bookId);
                    req.setAttribute("book", book);
                    req.getRequestDispatcher("editBook.jsp").forward(req, resp);
                    break;
                case "delete" :
                    if(req.getParameter("commit") == null) {
                        resp.sendRedirect("HomeServlet");
                    }
                    Long id = Long.parseLong(req.getParameter("commit"));
                    Book reqBook = bookService.getBook(id);
                    req.setAttribute("book1", reqBook);
                    req.getRequestDispatcher("deleteBook.jsp").forward(req, resp);
                    break;
                case "show" :
                    if(req.getParameter("commit") == null) {
                        resp.sendRedirect("HomeServlet");
                    }
                    req.getRequestDispatcher("ShowBookServlet").forward(req, resp);
                    break;
                    default:
                        resp.sendRedirect("home.jsp");
                        break;
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Book> books = bookService.getBooksList();
        req.setAttribute("books", books);
        req.getRequestDispatcher("home.jsp").forward(req, resp);
    }
}
